#ifndef TABELA_H
#define TABELA_H
#include <map>
#include <string>
#include <iostream>
#include "valor.h"
#include "util.h"

/**
 * Tabela com 2 campos: chave->valor.
 */
class Tabela
{
public:
   Tabela();
   ~Tabela();
   
   int read(int chave);
   void   write(int chave, int valor);
   
   int readSafe(int chave);
   void   writeSafe(int chave, int valor);
   
   void lock() { mMutex.lock(); }
   void unlock() { mMutex.unlock(); }
protected:   
   void insert(int chave, Valor* valor)
   { 
      tabela.insert(pair<int, Valor*>(chave, valor));       
   }
   
private:
    map<int, Valor*>tabela;
    mutex mMutex;
};
#endif // TABELA_H
