#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <sys/time.h>
#include <memory>
#include <thread>
#include <mutex>
#include <functional>

#define MAX 10000

using namespace std;

long int myRand();

class Transacao;
class MemoriaTransacional;


typedef shared_ptr<Transacao> TransacaoPtr;
typedef shared_ptr<MemoriaTransacional> MemPtr;


#endif