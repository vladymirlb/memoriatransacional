#include "tabelagrosseira.h"

/**
 * Construtor da tabela. Inicia com 1000 valores <0..999>.
 * Inicialmente chave == valor.
 */
TabelaGrosseira::TabelaGrosseira()
{
   for(int i=0; i<MAX; i++) {
      this->insert(i, new Valor(i));
   }
}

/**
 * Destroi os objetos Valor* da tabela;
 */
TabelaGrosseira::~TabelaGrosseira()
{
   for(int i=0; i<MAX; i++)
      delete tabela[i];
}

int TabelaGrosseira::read(int chave) 
{
   if( chave < 0 || chave >= this->tabela.size() )
      throw;
   return tabela[chave]->readSafe();
}

void TabelaGrosseira::write(int chave, int valor)
{
   if( chave < 0 || chave >= this->tabela.size() )
      throw;
   tabela[chave]->writeSafe(valor);
}
