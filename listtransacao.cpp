#include "listtransacao.h"
#include "transacao.h"

ListTransacao::ListTransacao(): counterUp(0), counterDown(0)
{
   this->mLista = new list<Transacao*>();
   fila = new queue<Transacao*>();
}

ListTransacao::~ListTransacao()
{
   delete mLista;
   delete fila;
}

Transacao* ListTransacao::pop()
{
   if(mLista->size() == 0)
      return 0;
   --counterDown;
   if(counterDown >= counterUp)
      exit(0);
   Transacao* trans = mLista->front();
   mLista->pop_front();
   return trans;
}

void ListTransacao::push_and_notify(Transacao* tx)
{
   unique_lock<mutex> lk(mMutex);
   fila->push(tx);
   lk.unlock();
   varCondicional.notify_all();
   
}

Transacao* ListTransacao::wait_and_pop() 
{
   Transacao *tx = 0;
   unique_lock<mutex> lk(mMutex);
   //while(fila->empty()){
      varCondicional.wait(lk, [&fila](){ return !fila->empty(); });
   //�/}//
   tx = fila->front();
   fila->pop();
   lk.unlock();
   return tx;
}