#ifndef TRANSACAO_H
#define TRANSACAO_H

#include <map>
#include <list>
#include <string>
#include <mutex>
#include <time.h>


#include "valor.h"
#include "util.h"

#define READ 1
#define WRITE 2

class MemoriaTransacional;

struct pacote {
   int chave;
   int valor;
   long versao;
};

typedef struct pacote Pkg;
/**
 * Uma transacao dentro da memoria transacional. Armazena um conjunto de 
 * chamadas (read, write)
 */
class Transacao
{

public:
    Transacao();
    Transacao(const Transacao& other);
    virtual ~Transacao();
    
    int read(int chave);
    void write(int chave, int valor);
    
    long getId() { return id; }
    void commit();
    
    void setId(long id) { this->id = id; }
    
    list< pair<  int, Pkg > >& getChamadas() { return listaDeChamadas; }
    
    void fim();
    
    void show();
    static long count;
private:
   void enq( pair<  int, Pkg >);
   list< pair<  int, Pkg > > listaDeChamadas;
   long id;
   std::mutex mMutex;
};

#endif // TRANSACAO_H
