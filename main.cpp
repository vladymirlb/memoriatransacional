#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "listtransacao.h"
#include "memoriatransacional.h"
#include "tabelagrosseira.h"
#include "util.h"
#include <bits/shared_ptr_base.h>
#include "filasincronizada.h"

void runThread();
void runThreadGranular(Tabela* tabela);
//long myRand();
void runThreadGrossa(TabelaGrosseira* tabela);
void consumer(int numTransacoes);


int main(int argc, char **argv) {
   /*struct timeval tv1, tv2;
   gettimeofday(&tv1, NULL);
   

   /**
    * Criando 30 threads
    *
   for(int i=0; i<30; i++) {
       //thread tx(runThreadGrossa, tabela);
       thread tx(runThread);
       tx.join();
   }
   thread t1(consumer, 30);
   t1.join();
   pair<int,int> result =  MemoriaTransacional::getInstancia()->stats();
   cout << "Abortos: " << result.first << "\nSucessos: " << result.second << endl;

   /**
    * Dando lock Global na tabela e executando
    *
   TabelaGrosseira* tabela = new TabelaGrosseira();
   for(int i=0; i<30; i++) {
       thread tx(runThreadGrossa, tabela);
       tx.join();
   }
   delete tabela; */
   
  /* Tabela* tabela = new Tabela();
   for(int i=0; i<30; i++) {
      thread tx(runThreadGranular, tabela);
      tx.join();
   }
   
   gettimeofday(&tv2, NULL);
   cout << tv2.tv_usec - tv1.tv_usec << endl;*/
  int a = 10;
  int *b = &a;
  FilaSincronizada<int> filepa;
  filepa.push(*b);
  a = 20;
  cout << filepa.pop() << endl;
   return 0;
}

/**
 * Funcao da Thread. Cria 3 transacoes com 9 reads e 1 write 
 * em posicoes aleatorias da tabela.
 * obs.: varios produtores para um consumidor
 */
void runThread()
{
   MemoriaTransacional* mem = MemoriaTransacional::getInstancia();
   ListTransacao* lista = MemoriaTransacional::getInstancia()->getTransacoes();
   
      int valor = 0;
      Transacao* tx = new Transacao();
      tx->setId(myRand());
      unsigned int dormir = 50000;
      valor += tx->read(myRand()%MAX);
      //usleep(dormir);
      valor += tx->read(myRand()%MAX);
      //usleep(dormir);
      valor += tx->read(myRand()%MAX);
     // usleep(dormir);
      valor += tx->read(myRand()%MAX);
      //usleep(dormir);
      valor += tx->read(myRand()%MAX);
      valor += tx->read(myRand()%MAX);
      valor += tx->read(myRand()%MAX);
      valor += tx->read(myRand()%MAX);
      tx->write(myRand()%MAX, valor);
      usleep(dormir);
      //unique_lock<mutex> lk(lista->mMutex);   
      //lista->varCondicional.wait(lk, [&lista] { return !lista->mMutex.try_lock();
         /*return !lista->mMutex.try_lock();});*/
      lista->push_and_notify(tx);

      //lk.unlock();
      //lista->varCondicional.notify_all();  
}

/**
 * Thread que vai processar as transacoes e verificar se sera 
 * possivel commitar ou abortar.
 */
void consumer(int numTransacoes)
{
   MemoriaTransacional* mem = MemoriaTransacional::getInstancia();
   ListTransacao* lista = mem->getTransacoes();
   while(true) {
      //unique_lock<mutex> lk(lista->mMutex);
      //lista->varCondicional.wait(lk, [&lista]{ return !lista->mMutex.try_lock() ||
                                                      //!lista->isEmpty();});
      
      Transacao *tx = lista->wait_and_pop();//lista->pop();
      cout << "Transacao: " <<endl;
      tx->show();
      if(tx == 0) {
         //lk.unlock();
         //lista->varCondicional.notify_all();
         delete tx;         
         continue;
      }
      --numTransacoes;
      
      mem->tryToCommit(tx);
                
      if(numTransacoes == 0) {
         //lk.unlock();
         //lista->varCondicional.notify_all();
         break;         
      }     
      //lk.unlock();
      //lista->varCondicional.notify_all();
   }

}

/**
 * Funcao para gerar numeros aleatorios.
 */
long int myRand()
{
   struct timeval tv;
   gettimeofday(&tv, NULL);
   srand(tv.tv_usec);
   return rand();
}

void runThreadGrossa(TabelaGrosseira* tabela) 
{
   //tabela->lock();
   lock_guard<mutex> lk(tabela->mMutex);
      unsigned int dormir = 50000;
      //unique_lock<mutex> lk(tabela->mMutex);
      //tabela->myVar.wait(lk, [&tabela]{ return !tabela->mMutex.try_lock();});
      int valor = 0; 
      valor += tabela->read(myRand()%MAX);
      //usleep(dormir);
      valor += tabela->read(myRand()%MAX);
     // usleep(dormir);
      valor += tabela->read(myRand()%MAX);
      //usleep(dormir);
      valor += tabela->read(myRand()%MAX);
      //usleep(dormir);
      valor += tabela->read(myRand()%MAX);
      valor += tabela->read(myRand()%MAX);
      valor += tabela->read(myRand()%MAX);
      valor += tabela->read(myRand()%MAX);
      valor += tabela->read(myRand()%MAX);
      tabela->write(myRand()%MAX, valor);    
      usleep(dormir);
      //lk.unlock();
      //tabela->myVar.notify_all();
      
   //tabela->unlock();  
}

void runThreadGranular ( Tabela* tabela )
{
   int valor = 0;
   unsigned int dormir = 50000;
   valor += tabela->readSafe(myRand()%MAX);
   //usleep(dormir);
   valor += tabela->readSafe(myRand()%MAX);
   //usleep(dormir);
   valor += tabela->readSafe(myRand()%MAX);
   //usleep(dormir);
   valor += tabela->readSafe(myRand()%MAX);
   //usleep(dormir);
   valor += tabela->readSafe(myRand()%MAX);
   valor += tabela->readSafe(myRand()%MAX);
   valor += tabela->readSafe(myRand()%MAX);
   valor += tabela->readSafe(myRand()%MAX);
   valor += tabela->readSafe(myRand()%MAX);
   tabela->writeSafe(myRand()%MAX, valor);
   usleep(dormir);
}
