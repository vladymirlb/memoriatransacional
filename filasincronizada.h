#ifndef FILASINCRONIZADA_H
#define FILASINCRONIZADA_H

/**
 * Fila sincronizada generica.
 * @author: Vladymir Bezerra
 */
#include <mutex>
#include <thread>
#include <queue>

template <typename T>
class FilaSincronizada
{

public:
   FilaSincronizada();
   virtual ~FilaSincronizada();
   
   void push(const T& item);
   const T& pop();
private:
   std::queue<T> fila;
   
   std::mutex mMutex;
   std::condition_variable mCond;

   
};

template <typename T>
FilaSincronizada<T>::FilaSincronizada()
{}

template <typename T>
FilaSincronizada<T>::~FilaSincronizada()
{}

template <typename T>
const T& FilaSincronizada<T>::pop()
{
   std::unique_lock<std::mutex> lk(mMutex);
   if( fila.empty() )
      mCond.wait(lk, [&fila](){ return !fila.empty(); } );
   const T& item = fila.front();
   fila.pop();
   lk.unlock();
   return item;
}

template <typename T>
void FilaSincronizada<T>::push(const T& item)
{
   std::lock_guard<std::mutex> lk(mMutex);
   fila.push(item);
   mCond.notify_one();
}

#endif // FILASINCRONIZADA_H
