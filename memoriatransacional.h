#ifndef MEMORIATRANSACIONAL_H
#define MEMORIATRANSACIONAL_H

#include "tabela.h"
#include <time.h>

class Transacao;
class ListTransacao;

class MemoriaTransacional
{
public:
   static MemoriaTransacional* getInstancia();
   long int getVersaoOficial(int chave);
   long int getVersaoCorrente(int chave);
   void setVersaoOficial(int chave, long versao) { versaoOficial[chave] = versao; }
   void setVersaoCorrente(int chave, long versao) { versaoCorrente[chave] = versao; }
   Tabela* getTabela() { return tabela; }
   ListTransacao* getTransacoes() { return transacoes; }
   //void show() { for(int i=0;i<1000;i++) cout << versoes[i]->first<<endl; }
   
   bool tryToCommit(Transacao* tx);
   
   pair<int, int> stats() { return pair<int, int>(abortos, sucessos); }
private:
   void preparaVersoes();
   MemoriaTransacional();
   ~MemoriaTransacional();
   static MemoriaTransacional* memoria;
   ListTransacao* transacoes;
   Tabela* tabela;
   long int versaoOficial[MAX];
   long int versaoCorrente[MAX];
   
   int abortos, sucessos;
};

#endif // MEMORIATRANSACIONAL_H
