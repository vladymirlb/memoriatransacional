#include "transacao.h"
#include "memoriatransacional.h"

long Transacao::count = 0;

Transacao::Transacao()
{
   id = ++count;
}

Transacao::Transacao(const Transacao& other)
{
   
}

Transacao::~Transacao()
{

}

/**
 * Enfileira um par (nome_da_chamada, pacote com valores) 
 * na fila de chamadas da transacao.
 */
void Transacao::enq(  pair<  int, struct pacote > chamada)
{
   //this->listaDeChamadas.
   this->listaDeChamadas.push_back(chamada);
}

/**
 * Para cada chamada dentro da lista de chamadas, tenta
 * commitar cada operacao individualmente, se nao houver nenhuma versao mais
 * recente.
 */
void Transacao::commit()
{
   MemoriaTransacional *mem = MemoriaTransacional::getInstancia();
   list<  pair<  int , Pkg > >::iterator it;
   Pkg pkg;
   for(it = listaDeChamadas.begin(); it != listaDeChamadas.end(); it++) {
      if(it->first == READ) {
            mem->getTabela()->readSafe(it->second.chave);
      } else if(it->first == WRITE) {
         mem->getTabela()->writeSafe(it->second.chave, it->second.valor);
       
      }
   }
}


int Transacao::read ( int chave )
{
   Pkg pkg;
   pkg.chave = chave;
   MemoriaTransacional* mem = MemoriaTransacional::getInstancia();
   list< pair<  int, Pkg > >::iterator it;
   /** verifica as ultimas operacoes em busca de uma leitura ja efetuada,
    *  se sim retorna. Caso contrario l� da tabela.
    */
   for(it = listaDeChamadas.end(); it != listaDeChamadas.begin(); it--) {
      if(it->second.chave == chave) {
         pkg.valor = it->second.valor;
         pkg.versao = it->second.versao;
         listaDeChamadas.push_back( pair< int, Pkg>(READ, pkg));
         return pkg.valor;
      }
   }
   // read sincronizado
   pkg.valor = mem->getTabela()->readSafe(chave);
   // nao esta sincronizado - problem? [trollface]
   pkg.versao = mem->getVersaoOficial(chave);
   listaDeChamadas.push_back( pair< int, Pkg>(READ, pkg));
   return pkg.valor;
}

void Transacao::write ( int chave, int valor )
{
   MemoriaTransacional* mem = MemoriaTransacional::getInstancia();
   struct timeval myTime;
   Pkg pkg;
   pkg.chave = chave;
   pkg.valor = valor;
   gettimeofday(&myTime, NULL);
   pkg.versao = myTime.tv_usec;

   if(pkg.versao > mem->getVersaoCorrente(chave)) {
      mem->setVersaoCorrente(chave, pkg.versao);
   }
   listaDeChamadas.push_back( pair< int, Pkg>(WRITE, pkg));
}

void Transacao::show()
{
    list< pair<  int, Pkg > >::iterator it;
   for(it = listaDeChamadas.begin(); it!= listaDeChamadas.end(); it++) {
      Pkg pkg = it->second;
       cout << "metodo: " << it->first << " pacote: " << pkg.chave << " " <<
      pkg.valor << " " << pkg.versao <<  endl;
   }
}
