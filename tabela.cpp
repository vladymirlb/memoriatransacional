#include "tabela.h"

/**
 * Construtor da tabela. Inicia com 1000 valores <0..999>.
 * Inicialmente chave == valor.
 */
Tabela::Tabela()
{
   for(int i=0; i<MAX; i++) {
      this->insert(i, new Valor(i));
   }
}

/**
 * Destroi os objetos Valor* da tabela;
 */
Tabela::~Tabela()
{
   for(int i=0; i<MAX; i++)
      delete tabela[i];
}

int Tabela::read(int chave) 
{
   if( chave < 0 || chave >= this->tabela.size() )
      throw;

   int valor = tabela[chave]->read();

   return valor;
}

void Tabela::write(int chave, int valor)
{
   if( chave < 0 || chave >= this->tabela.size() )
      throw;

   tabela[chave]->write(valor);

}

int Tabela::readSafe(int chave) 
{
   if( chave < 0 || chave >= this->tabela.size() )
      throw;

   tabela[chave]->lock();
   int valor = tabela[chave]->read();
   tabela[chave]->unlock();
   return valor;
}

void Tabela::writeSafe(int chave, int valor)
{
   if( chave < 0 || chave >= this->tabela.size() )
      throw;
   tabela[chave]->lock();
   tabela[chave]->write(valor);
   tabela[chave]->unlock();

}