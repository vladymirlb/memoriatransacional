#include "memoriatransacional.h"
#include "listtransacao.h"
#include <sys/types.h>

MemoriaTransacional* MemoriaTransacional::memoria = NULL;

MemoriaTransacional::MemoriaTransacional(): abortos(0), sucessos(0)
{
   tabela = new Tabela();
   transacoes = new ListTransacao();
   preparaVersoes();
}

MemoriaTransacional::~MemoriaTransacional()
{
   delete tabela;
   delete transacoes;
}

MemoriaTransacional* MemoriaTransacional::getInstancia() {
   if(memoria == NULL) {
      memoria = new MemoriaTransacional();
      return memoria;
   }
   return memoria;
}

long int MemoriaTransacional::getVersaoOficial(int chave) {
   return versaoOficial[chave];
}

long int MemoriaTransacional::getVersaoCorrente(int chave) {
   return versaoCorrente[chave];
}

void MemoriaTransacional::preparaVersoes ()
{
   struct timeval myTime;
   gettimeofday(&myTime, NULL);
   for(int i=0; i< MAX; i++) {
      versaoOficial[i] = myTime.tv_usec;
      versaoCorrente[i] = myTime.tv_usec;
   }
}

bool MemoriaTransacional::tryToCommit(Transacao* tx) 
{
   list<pair<int, Pkg>>::iterator it;
   for(it=tx->getChamadas().begin(); it!= tx->getChamadas().end(); it++) {
      if(it->first == READ) {
         if(it->second.versao < versaoCorrente[it->second.chave]) {
            abortos++;
            tabela->unlock();
            return false;
         } 
      }
      else if(it->first == WRITE) {
         if(it->second.versao < getVersaoCorrente(it->second.chave)) {        
            abortos++;
            tabela->unlock();
            return false;
         } 
      }
   }
   tx->commit();
   sucessos++;
   return true;
}