#ifndef TABELAGROSSEIRA_H
#define TABELAGROSSEIRA_H

#include <map>
#include <string>
#include <iostream>
#include <mutex>
#include "valor.h"
#include "util.h"

/**
 * Tabela com 2 campos: chave->valor.
 */
class TabelaGrosseira
{
public:
   TabelaGrosseira();
   ~TabelaGrosseira();
   
   int read(int chave);
   void   write(int chave, int valor);
   
   bool lock() { mMutex.lock(); };
   void unlock() { mMutex.unlock();};
   
   mutex mMutex;
   condition_variable myVar;
protected:   
   void insert(int chave, Valor* valor)
   { 
      tabela.insert(pair<int, Valor*>(chave, valor));       
   }
   
private:
   
    map<int, Valor*>tabela;
};

#endif