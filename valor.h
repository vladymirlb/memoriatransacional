#ifndef VALOR_H
#define VALOR_H

#include <string>
#include <mutex>
#include <unistd.h>
#include <memory>

#include "util.h"

/**
 * Representa um valor inteiro na tabela. Precisa-se desta classe wrapper
 * para a sincronizacao de grao fino.
 */
class Valor
{
public:
    Valor(int valor);
    Valor(const Valor &outro);
    
    virtual ~Valor();
     
    int read() { return mValor; }
    void write(int valor) { mValor=valor; }
    int readSafe();
    void writeSafe(int valor);
    
    void lock() { return mMutex.lock(); }
    bool try_lock() { return mMutex.try_lock(); }
    void unlock() { mMutex.unlock(); }
     shared_ptr<int> readSF();
    
    int& operator=(const int &valor) {
       this->mValor = valor;
   }
    
private:
   int mValor;
   mutex mMutex;
};

#endif // VALOR_H
