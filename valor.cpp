#include "valor.h"

/**
 * Construtor. Inicializa o valor.
 */
Valor::Valor(int valor): mValor(valor)
{}

/**
 * Construtor de copia. 
 * Nao sei se vai ser necessario (ate entao).
 */
/*Valor::Valor(const Valor& outro)
{
   lock_guard< mutex>lock(outro.mMutex);
   this->mValor = outro.mValor;
}*/


Valor::~Valor()
{}

/**
 * Le o valor de fato.
 * Metodo sincronizado.
 */
int Valor::readSafe()
{
    lock_guard< mutex>lock(mMutex);
   return mValor;
}

/**
 * Escreve um valor.
 * Metodo sincronizado.
 */
void Valor::writeSafe(int valor)
{
    lock_guard< mutex> lock(mMutex);
   mValor = valor;
}

/**
 * Retorna um shared_pointer para o valor inteiro.
 * Nao sei se vai ser necessario.
 */
 shared_ptr< int > Valor::readSF()
{
    lock_guard< mutex> lock(mMutex);
    shared_ptr<int> const res(new int(mValor));
   return res;
}