#ifndef LISTTRANSACAO_H
#define LISTTRANSACAO_H

#include <list>
#include <queue>
#include <mutex>
#include <thread>

#include "transacao.h"

class Transacao;

class ListTransacao
{
public:
    ListTransacao();
    //ListTransacao(int count);
    virtual ~ListTransacao();
    
    void push_back(Transacao* tx) { mLista->push_back(tx); counterUp++; };
    Transacao* pop();
    
    list<Transacao*>* getTransacoes() { return this->mLista; };
    void push_and_notify(Transacao *tx);
    Transacao* wait_and_pop();
    void lock();
    void unlock();
    int size() { return mLista->size(); }
    bool isEmpty() { return mLista->empty(); }
    
    
    
    mutex mMutex;
    condition_variable varCondicional;

private:
   queue<Transacao*>* fila;
   list<Transacao*>* mLista;
   int counterUp;
   int counterDown;
};

#endif // LISTTRANSACAO_H
